$(document).ready(function() {

    var owl = $("#slider");

    $("a[href='#top']").on('click touch', function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

    $("#slider").owlCarousel({
        items: 1,
        responsive: false,
        nav: true
    });

    $(".glyphicon-search").on('click touch', function() {
        $(".search-wrapper").toggleClass('active');
        $(".social-fixed").addClass('hidden');
        $(".glyphicon-search").toggleClass('active');
    })

    $(".glyphicon-user").on('click touch', function() {
        $(".user-wrapper").toggleClass('active');
        $(".social-fixed").addClass('hidden');
        $(".glyphicon-user").toggleClass('active');
    })

    $(document).mouseup(function(e) {

        var user = $(".user-wrapper");
        var search = $(".search-wrapper");

        if (!$(e.target).closest('.user-wrapper').length) {
            user.removeClass('active');
            $(".glyphicon-user").removeClass('active');
        }

        if (!$(e.target).closest('.search-wrapper').length) {
            search.removeClass('active');
            $(".glyphicon-search").removeClass('active');
        }
        $('.close-form').on('click touch', function() {
            $('.search-wrapper').removeClass('active')
            $('.user-wrapper').removeClass('active')
            $(".social-fixed").removeClass('hidden');
            $(".glyphicon-search").removeClass('active');
            $(".glyphicon-user").removeClass('active');
        })

    });

    $('.link').on('click touch', function() {
        $(this).closest('.car-block').find('a')[0].click();
    });
});
